# Sample multi functional project

### XXX token
https://rinkeby.etherscan.io/address/0xCC7C56a676A1b4bafdbDbd9Ac93554e57abBBBd0

### ACDM token
https://rinkeby.etherscan.io/address/0x9Fda078095Bd368be59A5c7F9aba688a6E528586

### Liquidity provider token
https://rinkeby.etherscan.io/token/0x70BDD02Fa5B4D453068284Fcd010e4162c97B2a5

### Staking contract
https://rinkeby.etherscan.io/address/0xD81a9c5F7DDc93Be261687d3c05156ac024E3aF1

### DAO contract
https://rinkeby.etherscan.io/address/0xe994921e3cdf9EAe5361ff67131207411E5357ef

### ACDM platform
https://rinkeby.etherscan.io/address/0x21eb640e3BDB3B9EB45cc3cC2de9695240B8c4d2

```sh

  DAO testing
    Add proposal
      ✔ Adding proposal #1 (50ms)
      ✔ Adding proposal #2 (48ms)
      ✔ Adding proposal #3 (44ms)
      ✔ Adding proposal #4 (52ms)
      ✔ Adding proposal #5 (65ms)
      ✔ Adding proposal #6 (41ms)
      ✔ Adding proposal with wrong calldata should be reverted
      ✔ Adding proposal from alien should be reverted (153ms)
    Vote
      ✔ Proposal #1. Voting by 3 participants (157ms)
        Proposal #1. YES votes 600
        Proposal #1. NO votes 0
      ✔ Proposal #1. Checking votes amount (44ms)
      ✔ Proposal #2. Voting by 6 participants (286ms)
        Proposal #2. YES votes 2100
        Proposal #2. NO votes 0
      ✔ Proposal #2. Checking votes amount (45ms)
      ✔ Proposal #3. Voting by 9 participants (390ms)
        Proposal #3. YES votes 4500
        Proposal #3. NO votes 0
      ✔ Proposal #3. Checking votes amount (51ms)
      ✔ Proposal #4. Voting by 12 participants (1057ms)
        Proposal #4. YES votes 6600
        Proposal #4. NO votes 1200
      ✔ Proposal #4. Checking votes amount (42ms)
      ✔ Proposal #5. Voting by 15 participants (664ms)
        Proposal #5. YES votes 6600
        Proposal #5. NO votes 5400
      ✔ Proposal #5. Checking votes amount (40ms)
      ✔ Proposal #6. Voting by 18 participants (762ms)
        Proposal #6. YES votes 6600
        Proposal #6. NO votes 10500
      ✔ Proposal #6. Checking votes amount (38ms)
      ✔ Unstaking for voting user should be reverted
      ✔ Voting without stakes should be reverted
      ✔ Voting for non-exist proposal should be reverted
      ✔ Second voting should be reverted
      ✔ Too early finishing should be reverted
      ✔ Too late voting should be reverted
    Finish
      ✔ Proposal #1 without quorum should by cancelled
      ✔ Second finishing should be reverted
      ✔ Proposal #2. Should call recipient, call should be success
      ✔ Proposal #3. Should call recipient, call should be failed
      ✔ Proposal #4. Platform ACDM sale round percents changing, call should be success
      ✔ Proposal #5. Platform ACDM trade round percents changing, call should be success
      ✔ Proposal #6 with minority of YES votes should be cancelled
      ✔ Unstaking for voted user should be success after proposal finish

  ACDM platform
    Registration
      ✔ 6 users should be registered (164ms)
      ✔ Registration with non-exist user should be reverted
      ✔ Second registration of the same user should be reverted
    Sale Round 1
      ✔ Start sale round (75ms)
      ✔ Second start of sale round should be reverted
      ✔ Buying too much ACDM tokens should be reverted (41ms)
      ✔ Too early trade start should be reverted
      ✔ Buying ACDM tokens with checking token balance and referral rewards (425ms)
      ✔ Adding order in sale round should be reverted
    Trade Round 1
      ✔ Start trade round
      ✔ Second start of trade round should be reverted
      ✔ 4 orders should be added (373ms)
      ✔ Buying too much ACDM tokens should be reverted
      ✔ Too early sale start should be reverted
      ✔ Removing order by alien should be reverted
      ✔ Removing one order (38ms)
      ✔ Trying to by removed order should be reverted
      ✔ 3 orders should be redeemed partly (222ms)
    Sale Round 2
      ✔ Start sale round. Minted tokens amount should be calculated from trade volume (60ms)
    Trade Round 2
      ✔ Start trade round (53ms)
      ✔ 3 orders should be redeemed fully (199ms)

  Staking
    ✔ Should staked (84ms)
    ✔ Should not rewarded too soon
    ✔ Should stake added (50ms)
    ✔ Should rewarded (63ms)
    ✔ Should not unstaked too soon
    ✔ Should not unstaked if zero balance
    ✔ Unstake should be success (51ms)
    ✔ Staking parameters should be changed (58ms)


  63 passing (11s)

-------------------|----------|----------|----------|----------|----------------|
File               |  % Stmts | % Branch |  % Funcs |  % Lines |Uncovered Lines |
-------------------|----------|----------|----------|----------|----------------|
 contracts/        |      100 |      100 |      100 |      100 |                |
  DAO.sol          |      100 |      100 |      100 |      100 |                |
  PlatformACDM.sol |      100 |      100 |      100 |      100 |                |
  Staking.sol      |      100 |      100 |      100 |      100 |                |
  Token20.sol      |      100 |      100 |      100 |      100 |                |
-------------------|----------|----------|----------|----------|----------------|
All files          |      100 |      100 |      100 |      100 |                |
-------------------|----------|----------|----------|----------|----------------|

```